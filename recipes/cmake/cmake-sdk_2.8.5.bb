inherit sdk
require cmake.inc

PR = "${INC_PR}.0"

SRC_URI[md5sum] = "3c5d32cec0f4c2dc45f4c2e84f4a20c5"
SRC_URI[sha256sum] = "5e18bff75f01656c64f553412a8905527e1b85efaf3163c6fb81ea5aaced0b91"

do_configure_append () {
        sed -e 's/BUILD_CursesDialog:BOOL=ON/BUILD_CursesDialog:BOOL=OFF/' \
                -i CMakeCache.txt
}
