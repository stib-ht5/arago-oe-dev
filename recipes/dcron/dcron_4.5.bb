SECTION = "base"
DESCRIPTION = "Dillon's Cron is a multi-user cron written from scratch, \
similar to vixie-cron but with major differences."
LICENSE = "GPL"
PR = "r2"

SRC_URI = "http://www.jimpryor.net/linux/releases/dcron-${PV}.tar.gz"

do_install () {
	install -d ${D}${sysconfdir}/cron.d
	install -d ${D}${bindir} ${D}${sbindir} \
		   ${D}${mandir}/man1 ${D}${mandir}/man8
	install -m 0755 crond ${D}${sbindir}/
	install -m 4755 crontab ${D}${bindir}/
	install crontab.1 ${D}${mandir}/man1
	install crond.8 ${D}${mandir}/man8
}

SRC_URI[md5sum] = "078833f3281f96944fc30392b1888326"
SRC_URI[sha256sum] = "9e50edb6f5bd8153b16bad05087d985e5153ce45cc01ae77e7f842213fb4a824"
