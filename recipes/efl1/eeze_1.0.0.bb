require eeze.inc

PR = "${INC_PR}.0"

SRC_URI = "\
  ${E_MIRROR}/${SRCNAME}-${PV}.tar.gz \
"

#SRC_URI[md5sum] = "31f9b2a8fce56a73c99c1c5d2a449fb6"
#SRC_URI[sha256sum] = "27da2b12f60b3c23cd52d16d601b5ab61734b9038beb3ab40129ac770f048d84"

SRC_URI[md5sum] = "8fa2aa89ba48f262700a3cf88cb1c777"
SRC_URI[sha256sum] = "d8ffb1af53c03c5860cd90ae78aa5ae0ce2d027a462509deaca5ee3adacd0cee"
