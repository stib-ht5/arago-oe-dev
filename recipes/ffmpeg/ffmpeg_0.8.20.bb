require ffmpeg.inc

LICENSE = "LGPLv2.1+"
DEPENDS += "schroedinger libgsm"
PR = "${INC_PR}.1"

SRC_URI = "https://libav.org/releases/libav-${PV}.tar.gz"
SRC_URI[md5sum] = "762d87d11f38a043e05b6617afa20d56"
SRC_URI[sha256sum] = "7f1a5b466ceacf830f5f521013516a899745713ef1aa2f18f0c05b0ba5a1ea56"

SRC_URI += "file://compile.patch"

S = "${WORKDIR}/libav-${PV}"

EXTRA_FFCONF_armv7a = "--cpu=cortex-a8"
EXTRA_FFCONF_mipsel = "--arch=mips"

EXTRA_OECONF = " \
        --arch=${TARGET_ARCH} \
        --cross-prefix=${TARGET_PREFIX} \
        --enable-cross-compile \
        --enable-libgsm \
        --enable-libmp3lame \
        --enable-libschroedinger \
        --enable-libtheora  \
        --enable-libvorbis \
        --enable-pthreads \
        --enable-shared \
        --enable-swscale \
        --extra-cflags="${TARGET_CFLAGS} ${HOST_CC_ARCH}${TOOLCHAIN_OPTIONS}" \
        --extra-ldflags="${TARGET_LDFLAGS}" \
        --sysroot="${STAGING_DIR_TARGET}" \
        --prefix=${prefix}/ \
        --target-os=linux \
        ${EXTRA_FFCONF} \
"

do_configure() {
	set -x
        ./configure ${EXTRA_OECONF}
	set +x
}

FULL_OPTIMIZATION_armv7a = "-fexpensive-optimizations  -ftree-vectorize -fomit-frame-pointer -O4 -ffast-math"
BUILD_OPTIMIZATION = "${FULL_OPTIMIZATION}"
