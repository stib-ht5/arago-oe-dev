#!/bin/sh

# our mount.nfs doesn't assume -n when /etc/mtab is a symlink
# to /proc/mounts so we have to force it or autofs will fail
# to mount.
#
if [ $# -ge 2 ]; then
	remotetarget="$1"
	dir="$2"
	if [ -d "$dir" ]; then
		shift 2
		set -- "$remotetarget" "$dir" -n "$@"
	fi
fi
exec /sbin/mount.nfs.real "$@"
