DESCRIPTION = "Embeddable SQL database engine"
SECTION = "libs"
PRIORITY = "optional"
DEPENDS = "readline ncurses"
DEPENDS_virtclass-native = "tcl-native readline-native ncurses-native"
INC_PR = "r0"

def sqlite_download_version(d):
    pvsplit = bb.data.getVar('PV',d, 1).split('.')
    if len(pvsplit) < 4:
        pvsplit.append('0')
    return pvsplit[0] + ''.join([part.rjust(2,'0') for part in pvsplit[1:]])

SQLITE_PV = "${@sqlite_download_version(d)}"

S = "${WORKDIR}/sqlite-autoconf-${SQLITE_PV}"

inherit autotools pkgconfig

BBCLASSEXTEND = "native"

nolargefile = "${@base_contains('DISTRO_FEATURES', 'largefile', '', '-DSQLITE_DISABLE_LFS', d)}"
EXTRA_OECONF = "--disable-tcl --enable-shared \
		--enable-threadsafe \
		--disable-static-shell \
		CFLAGS='${CFLAGS} ${nolargefile}' \
		"

CFLAGS_append = " -fPIC"

# pread() is in POSIX.1-2001 so any reasonable system must surely support it
BUILD_CFLAGS += "-DUSE_PREAD"
TARGET_CFLAGS += "-DUSE_PREAD"

# Provide column meta-data API
BUILD_CFLAGS += "-DSQLITE_ENABLE_COLUMN_METADATA"
TARGET_CFLAGS += "-DSQLITE_ENABLE_COLUMN_METADATA"

PACKAGES = "libsqlite libsqlite-dev libsqlite-doc sqlite3 sqlite3-dbg"
FILES_sqlite3 = "${bindir}/*"
FILES_libsqlite = "${libdir}/*.so.*"
FILES_libsqlite-dev = "${libdir}/*.a ${libdir}/*.la ${libdir}/*.so \
		       ${libdir}/pkgconfig ${includedir}"
FILES_libsqlite-doc = "${docdir} ${mandir} ${infodir}"
AUTO_LIBNAME_PKGS = "libsqlite"
